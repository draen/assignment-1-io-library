; syscalls
%define    EXIT    60
%define    READ    0
%define    WRITE   1

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    ; rdi: exit code
    mov rax,EXIT
    syscall
    ret     ;technically not needed

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    ; rdi: string pointer
    mov rax, -1
.loop:
    inc rax
    test byte [rdi+rax], 0xff   ;check if byte is null-terminator
    jne .loop
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
; easier to not use print_string for print_char or vice-versa
    ; rdi: string pointer
    push    rdi             ;save caller-saved register
    call    string_length   ;string_length -> rax
    pop     rsi             ;string pointer (rdi -> rsi)
    mov     rdi, 1          ;stdout descriptor
    mov     rdx, rax        ;string length
    mov     rax,WRITE
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    ;cool tricks to not use jump/call by replacing print_newline to be directly before print_char

; Принимает код символа и выводит его в stdout
print_char:
    ; rdi: char code
    push    di          ;cannot push 1 byte. Decided to just push 2 bytes instead of doing a manual push by doing decrement of rsp and saving 1 byte
    mov     rsi, rsp    ;string pointer
    mov     rdi,1       ;stdout descriptor
    mov     rdx,1       ;'string' length (1 char)
    mov     rax,WRITE
    syscall
    add rsp, 2          ;deallocate memory
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    ;rdi: int
    test  rdi, rdi  ;check if number is signed
    jns print_uint     ;if not - print_uint
    push rdi           ;if yes - print '-' sign & negate the number, then print_uint
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    ;same trick again - moving func to right before print_uint
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ;rdi: uint
    push word 0             ;null-terminator for a string
    mov rax, rdi            ;move uint to rax
    mov rdi, 0xA            ;base10 for division
    mov r8, rsp             ;save current stack value (address of null-terminator)
.loop:
    dec rsp                 ;using rsp as a pointer to where in a string next digit goes (going decrementally)
    ; I was debating allocating like 20 bytes at the beginning and using other reg as pointer instead of doing that,
    ; but decided the difference wasn't that big

    xor rdx,rdx             ;clear rdx
    div rdi                 ;divide by 10 (using div r/m64 which just was easier to use)
    add dl, 0x30            ;get ASCII code of a digit
    mov [rsp], dl           ;save digit to string
    test rax,rax            ;continue division or exit
    jne .loop
.end:
    mov rdi, rsp            ;move address of first digit to rdi to serve as an argument for print_string
    push r8
    call print_string       ;do print and not forget the caller-saved register
    pop r8
    mov rsp, r8             ;de-allocate the memory
    pop di                  ;remove null-ender
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ;   rdi - string pointer 1
    ;   rsi - string pointer 2
    xor rax,rax
    xor rdx,rdx
.loop:
    mov al, [rdi]   ;load 1 byte from each string
    mov dl, [rsi]
    cmp al, dl      ;if bytes aren't equal - fail
    jne .fail
    inc rdi
    inc rsi
    test al,al      ;if the bytes are null-terminator - success
    jnz .loop
.exit:
    mov rax, 1
    ret
.fail:
    xor rax,rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rdi,rdi     ;stdin (0)
    dec rsp         ;create buffer for 1 byte
    mov rsi, rsp    ;buf addr
    mov byte [rsi], 0   ;a fix(?) for an issue with getting no symbol (before read_char would return a previously read symbol)
                    ;from what I understood some keyboard inputs would not actually be put in memory & we would get some old trash-value
    mov rdx, 1      ;num of bytes
    xor rax,rax     ;READ syscall
    syscall
    mov al, [rsi]   ;load byte in al
    inc rsp         ;deallocate buf
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:      ; this one is a mess
    ;rdi: buffer address
    ;rsi: buffer size
    push r12         ; using calle-saved registries because I don't what to push-pop in a loop with caller-saved ones
    push r13
    push r14
    mov r12, rdi     ; initial buffer address saved
    mov r13, rdi     ; symbol address counter
    mov r14, rsi     ; size counter
; I didn't really understand what was supposed to be the end of the word so I decided to count any whitespace as end
.skip_spaces:
    call read_char              ;char -> rax
    test rax,rax                ;extra check to see if we're getting end of word immediately (a certified `test.py` moment)
    jz .end
    call .read_or_whitespace    ;read 1 symbol & if it's a whitespace loop and read again
    test rax,rax
    jz .skip_spaces
    jmp .save                   ;if it's not a whitespace go save it
.main_loop:
    call read_char              ;char -> rax
    call .read_or_whitespace    ;read symbol. If it's whitespace - end of word, exit
    test rax,rax
    jz .end
.save:
    mov [r13], al                ;move symbol to buffer
    inc r13                      ;increase counter
    dec r14                      ;decrease remaining size
    test r14,r14                 ;if buffer ended - overflow, otherwise - continue reading
    jnz .main_loop
.overflow:
    xor rax,rax                 ;0 -> rax
    jmp .exit
.end:
    mov byte [r13], 0           ;put null-terminator
    mov rax, r12                ;put buffer address as return value
    sub r13, r12                ;calc length & put it as 2nd return value
    mov rdx, r13
.exit:
    pop r14                     ;restore callee-saved registries
    pop r13
    pop r12
    ret
.read_or_whitespace:    ;func to check if symbol is whitespace. Will return 0 if it is whitespace & will return symbol if it is not
    ; call read_char    ;can't put read_char here because of issues with getting 0x0 as first ever input
    cmp al, 0x20
    je .whitespace
    cmp al, 0x9
    je .whitespace
    cmp al, 0xA
    je .whitespace
    cmp al, 0x0         ; technically 0x0 isn't whitespace, but it works as end of the word regardless
    je .whitespace
    ret
.whitespace:
    xor rax,rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
; ok so apparently I misread the task and thought the whole string had to be a uint, not just the beginning. 
; Now it's fixed, but figuring that out was a wild ride
    ;rdi: string address
    mov r8, rdi         ; address counter
    mov r10, rdi        ; save start address for future use
    mov r9, 0xA         ; base 10 for converting
    xor rax,rax         ; clear rax (will be used for uint calculation)
    xor rdi,rdi
.loop:
    mov dil, [r8]       ; load next symbol and check it
    cmp dil, 0x30       ; just check that 30<=symbol<=39 (ASCII 0-9 numbers)
    js .exit
    cmp dil, 0x40
    jns .exit
    sub rdi, 0x30       ; convert to actual number
.calc:
    inc r8              ; increment address counter
    mul r9              ; do the `sum*10 + num` thing
    add rax, rdi
    jc .fail            ; if no carry flag after calc & no stuff in rdx after MUL - continue, otherwise fail - number bigger than 8 bytes uint
    test rdx,rdx
    jz .loop           
.fail:
    xor rdx,rdx         ;0->rdx
    ret
.exit:
    sub r8,r10          ; calc length & put it in rdx
    mov rdx,r8
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ; rdi - string address
    push r12            ;save r12 & r13 (decided to use callee-saved registers) 
    push r13
    xor r12,r12         ;clear r12 & r13
    xor r13,r13
    mov al, [rdi]       ;load 1st symbol
    cmp al, '+'         ;if 1st symbol is plus - skip it and do `parse_uint`
    je .skip_sign
    cmp al, '-'         ;if 1st symbol is '-' - skip it & save the info that number is negative, then do `parse_uint`
    jne .uint           ;if no +/- symbol at the beginning - just do `parse_uint`
    inc r12             ;use r12 as kind of boolean (if num is negative r12=1) 
.skip_sign:
    inc rdi             ; +1 to string address (so parse_uint starts after +/- sign)
    inc r13             ;use r13 as kind of boolean (if num had sign r13=1) 
.uint:
    call parse_uint     ;num -> rax, len -> rdx
    test rdx,rdx        ;if parse_uint failed (or has 0 length) - fail
    jz .fail
    test rax,rax        ;if uint takes the sign bit - fail (number too large for int but small enough for uint)
    js .fail
    test r13,r13        ;if r13 isn't 0 - there was a sign at the beginning of the number, need to do +1 to length
    jz .check_neg
    inc rdx             ;+1 to length
.check_neg:
    test r12,r12        ;if r12 is 0 - number positive, exit
    jz .exit
    neg rax             ;otherwise - negate the number
.exit:
    pop r13
    pop r12
    ret
.fail:
    xor rdx,rdx
    jmp .exit

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ;   rdi - string pointer
    ;   rsi - buffer pointer
    ;   rdx - buffer length
    push rdi
    push rsi
    push rdx
    call string_length      ; string length -> rax
    pop rdx
    pop rsi
    pop rdi

    cmp rax,rdx             ; if string length is greater than or EQUAL TO buffer length - overflow
    jge .overflow           ; string_length doesn't count null-terminator so to actually fit the string in buffer we need (buffer length >= string length + 1)
    xor rcx,rcx             ; clear rcx to use it as transfer-register
.loop:
    mov cl, [rdi]           ; copy 1 byte
    mov [rsi], cl
    inc rdi                 ; increment pointers
    inc rsi
    test rcx,rcx            ; if copied byte was null terminator - exit
    jnz .loop
.exit:
    ret                     ; string length already stored in rax as return value
.overflow:
    xor rax,rax
    ret
